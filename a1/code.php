<?php
 
class GetterSetter {
	public function get_name(){
		return $this->name;
	}

	public function set_name($name){
		$this->name = $name;
	}

	public function get_floors(){
		return $this->floors;
	}

	public function get_address(){
		return $this->address;
	}
}

class Building extends GetterSetter {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}

class Condominium extends Building {

}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");