<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S04 - Activity</title>
</head>
<body>
	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->get_name(); ?>.</p>
	<p>The <?= $building->get_name(); ?> has <?= $building->get_floors(); ?> floors.</p>
	<p>The <?= $building->get_name(); ?> is located at <?= $building->get_address(); ?>.</p>
	<?php $building->set_name("Caswynn Complex") ?>
	<p>The name of the building has been changed to <?= $building->get_name(); ?>.</p>

	<h1>Condominium</h1>
	<p>The name of the condominium is <?php echo $condominium->get_name(); ?>.</p>
	<p>The <?= $condominium->get_name(); ?> has <?= $condominium->get_floors(); ?> floors.</p>
	<p>The <?= $condominium->get_name(); ?> is located at <?= $condominium->get_address(); ?>.</p>
	<?php $condominium->set_name("Enzo Tower") ?>
	<p>The name of the condominium has been changed to <?= $condominium->get_name(); ?>.</p>


</body>
</html>