<?php

//[SECTION] Access Modifiers
//Each property and method inside of a class can be given a certain acess modifier

//public means that the property/method is accessible to all and can be reassigned/changed by anyone

//private means that direct access to an object's property is disabled and cannot be changed by anyone
//it also means that inheritance if its properties and methods are disabled

//protected also disables direct access to an object's properties and methods but inheritance is still allowed
//take note that the protected access modifier is ALSO inherited to the child class

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}

//[SECTION] Encapsulation
//Using what are called getter methods and setter methods, we can implement encapsulation of an object's data
//Getters and setters serve as an intermediary in accessing or reassinging and object's  properties or methods
//Getters and setters work for both private and protected properties
//you do not always need a getter and setter for every property

class Condominium extends Building {
	public function get_name(){
		return $this->name;
	}

	public function set_name($name){
		$this->name = $name;
	}
}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");